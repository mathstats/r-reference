# R introduction and reference guide

*School of Mathematics and Statistics, University of Melbourne*

This guide was written to introduce students to the R statistical software
package, as well as act as a handy reference for the key features that we use
in our subjects.

It is hosted at:
<https://mathstats.pages.gitlab.unimelb.edu.au/r-reference/>

The original version of this guide comes from the [Statistics
(MAST20005)][mast20005] teaching material.  For many years, it was provided (in
PDF format) to students in their first lab class.

[mast20005]: https://handbook.unimelb.edu.au/subjects/mast20005

Across several years, this guide was shared amongst lecturers in the School and
used in teaching several subjects.

In Feb 2022, Damjan Vukcevic created this online version of the guide and
shared it with the other statistics staff and senior tutors.  The aim was to
replace all of the PDF versions with a single, centrally maintained version
that is easily accessible to all students and staff.


## Licence

[![Creative Commons License][cc-img]][cc]  
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0
International License][cc].

[cc]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-img]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
